


#include "TrigConfigSvc/L1TopoConfigSvc.h"
#include "TrigConfigSvc/LVL1ConfigSvc.h"
#include "TrigConfigSvc/DSConfigSvc.h"
#include "TrigConfigSvc/HLTConfigSvc.h"
#include "TrigConfigSvc/HLTJobOptionsSvc.h"
#include "TrigConfigSvc/TrigConfigSvc.h"
#include "TrigConfigSvc/TrigConfDataIOVChanger.h"

DECLARE_COMPONENT( TrigConf::TrigConfDataIOVChanger )

DECLARE_COMPONENT( TrigConf::L1TopoConfigSvc )
DECLARE_COMPONENT( TrigConf::LVL1ConfigSvc )
DECLARE_COMPONENT( TrigConf::HLTConfigSvc )
DECLARE_COMPONENT( TrigConf::HLTJobOptionsSvc )
DECLARE_COMPONENT( TrigConf::DSConfigSvc )
DECLARE_COMPONENT( TrigConf::TrigConfigSvc )

